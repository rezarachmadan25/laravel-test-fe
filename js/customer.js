new Vue({
    el: '#app',
    vuetify: new Vuetify(),
    data: {
        customers: '',
        customer:'',
        customer_id: 0,
        customer_name: '',
        customer_email: '',
        customer_password: '',
        customer_gender: '',
        customer_is_married: '',
        dialogDelete: false,
        dialogManage: false,
        dialogDetail: false,
        genders: [
            { value: 'L', text: 'Laki-Laki' },
            { value: 'P', text: 'Perempuan' }
          ],
          customer_is_married: "s" ,
        showPassword: false,
        selectedGender: null
    },
    created: function () {
        this.getCustomers()
    },
    methods: {
 
        //Get List Customer
        getCustomers: function () {
            axios.get('http://localhost:8000/api/customers')
                .then(res => {
                    this.customers = res.data.result;
                    console.log(res);
                })
                .catch(err => {
                    console.log(err);
                })
        },
 
        //Save and Update Customer
        saveCustomer: function () {

            if(this.customer_id == 0)
            {
                axios.post('http://localhost:8000/api/customers', {
                    name: this.customer_name,
                    email: this.customer_email,
                    password: this.customer_password,
                    gender: this.customer_gender.value,
                    is_married: ( this.customer_is_married == 's' ? 0 : 1 )
                })
                .then(res => {
                    this.getCustomers();
                    this.customer_id = 0;
                    this.customer_name = '';
                    this.customer_email = '';
                    this.customer_password = '';
                    this.customer_gender = '';
                    this.customer_is_married = '';
                    this.dialogManage = false;
                })
                .catch(err => {
                    //if there some errors
                    console.log(err);
                })

            } else {

                axios.put(`http://localhost:8000/api/customers/${this.customer_id}`, {
                    name: this.customer_name,
                    email: this.customer_email,
                    password: this.customer_password,
                    gender: (typeof this.customer_gender == "object") ? this.customer_gender.value : this.customer_gender,
                    is_married: ( this.customer_is_married == 's' ? 0 : 1 )
                })
                .then(res => {
                    this.getCustomers();
                    this.dialogManage = false;
                })
                .catch(err => {
                     //if there some errors
                    console.log(err);
                })
                
            }

            
        },
 
        // Show Form Custome with Data
        getShowForm: function (customer = null) {
            this.dialogManage = true;
            if(customer != null)
            {
                this.customer_id = customer.id;
                this.customer_name = customer.name;
                this.customer_email = customer.email;
                this.customer_password = '';
                this.customer_gender = customer.gender;
                this.customer_gender = customer.gender;
                this.customer_is_married = customer.is_married == 1 ? 'm' : 's';
            }
            else
            {
                this.customer_id = 0;
                this.customer_name = '';
                this.customer_email = '';
                this.customer_password = '';
                this.customer_gender = '';
                this.customer_is_married = '';
            }
            
        },

        // Show Form Custome with Data
        getDetail: function (customer = null) {
            this.dialogDetail = true;
            this.customer = customer;
            
        },
 
        // Get Delete and Show Confirm Modal
        getDelete: function (customer = null) {
            this.dialogDelete = true;
            this.customer = customer;
        },
 
        // Delete customer
        deleteCustomer: function () {
            axios.delete(`http://localhost:8000/api/customers/${this.customer.id}`)
                .then(res => {
                    this.getCustomers();
                    this.dialogDelete = false;
                    this.customer = '';
                })
                .catch(err => {
                    // handle error
                    console.log(err);
                })
        }
    }
})
